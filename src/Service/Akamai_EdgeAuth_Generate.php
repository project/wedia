<?php

namespace Drupal\wedia\Service;

use Drupal\wedia\Service\Akamai_EdgeAuth_Config;

/**
 * Akamai token authentification generator.
 */
class Akamai_EdgeAuth_Generate {

  /**
   * Encrypt.
   *
   * @param string $str
   *   The string to encrypt.
   *
   * @return string
   */
  protected function h2b(string $str) : string {
    $bin = '';
    $i = 0;
    do {
      $bin .= chr(hexdec($str[$i] . $str[($i + 1)]));
      $i += 2;
    } while ($i < strlen($str));
    return $bin;
  }

  /**
   * Generate token.
   *
   * @param \Drupal\wedia\Service\Akamai_EdgeAuth_Config $config
   *   The token authentification configuration.
   *
   * @return string
   */
  public function generate_token(Akamai_EdgeAuth_Config $config) : string {
    // Assumes key is not null.
    $key = $config->get_key();
    // Assumes that algo = 'sha256'.
    $algo = $config->get_algo();
    // Assumes that filed_delimiter = '~'
    $field_delimiter = $config->get_field_delimiter();
    // Assumes that ip = ''.
    $m_token = $config->get_ip_field();
    // Assumes that start_time = null.
    $m_token .= $config->get_start_time_field();
    // Assumes that expr = ''.
    $m_token .= $config->get_expr_field();
    // Assumes that acl = '';
    $m_token .= $config->get_acl_field();
    // Assumes that session_id = ''.
    $m_token .= $config->get_session_id_field();
    // Assumes that data = ''.
    $m_token .= $config->get_data_field();
    // URL with all previous parameters.
    $m_token_digest = (string)$m_token;
    // Add URL as a URL parameter.
    $m_token_digest .= $config->get_url_field();
    // Assumes that salt = ''.
    $m_token_digest .= $config->get_salt_field();
    // Produce the signature and append to the tokenized string.
    $signature = hash_hmac(
      $algo,
      rtrim($m_token_digest, $field_delimiter),
      $this->h2b($key)
    );
    return $m_token . 'hmac=' . $signature;
  }

}
