<?php

namespace Drupal\wedia\Service;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Akamai token authentification configuration.
 */
class Akamai_EdgeAuth_Config {

  use StringTranslationTrait;

  /**
   * Wedia logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * @var bool
   */
  protected $debug = FALSE;

  /**
   * @var string
   */
  protected $algo = "SHA256";
  
  /**
   * @var string
   */
  protected $ip = '';
  
  /**
   * @var int
   */
  protected $start_time = 0;
  
  /**
   * @var int
   */
  protected $window = 300;
  
  /**
   * @var string
   */
  protected $acl = '';
  
  /**
   * @var string
   */
  protected $url = '';
  
  /**
   * @var string
   */
  protected $session_id = '';
  
  /**
   * @var string
   */
  protected $data = '';
  
  /**
   * @var string
   */
  protected $salt = '';
  
  /**
   * @var string
   */
  protected $key = 'aabbccddeeff00112233445566778899';
  
  /**
   * @var string
   */
  protected $field_delimiter = '~';
  
  /**
   * @var bool
   */
  protected $early_url_encoding = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->logger = $logger_factory->get('wedia');
  }

  /**
   * Encode a value.
   *
   * @param string $val
   *   The value to encode.
   *
   * @return string
   */
  protected function encode(string $val) : string {
    if ($this->early_url_encoding === TRUE) {
      return rawurlencode($val);
    }
    return $val;
  }

  /**
   * Set encryption algorithm.
   *
   * @param string $algo
   *   The algorithm name to use.
   */
  public function set_algo(string $algo) {
    if (in_array($algo, ['sha256','sha1','md5']) === TRUE) {
      $this->algo = $algo;
    }
    else {
      if ($this->debug === TRUE) {
        $msg = $this->t(
          'Invalid algorithme, must be one of "sha256", "sha1" or "md5".',
          [],
          ['context' => 'Wedia']
        );
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
    }
  }

  /**
   * Get encryption algorithm.
   *
   * @return string
   */
  public function get_algo() : string {
    return $this->algo;
  }

  /**
   * Set IP.
   *
   * @param string $ip
   *   IP address.
   */
  public function set_ip(string $ip) {
    $this->ip = $ip;
  }

  /**
   * Get IP.
   *
   * @return string
   */
  public function get_ip() : string {
    return $this->ip;
  }

  /**
   * Get IP URL parameter.
   *
   * @return string
   */
  public function get_ip_field() : string {
    if (empty($this->ip) === FALSE) {
      return 'ip=' . $this->ip . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set timestamp start time.
   *
   * @param int $start_time
   *   The timestamp where the time start.
   */
  public function set_start_time(int $start_time) {
    if (strcasecmp($start_time, 'now') == 0) {
      $this->start_time = time();
    }
    else {
      if ($start_time > 0 && $start_time < 4294967295) {
        $this->start_time = $start_time;
      }
      else {
        $msg = $this->t(
          'Start time input invalid or out of range.',
          [],
          ['context' => 'Wedia']
        );
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
    }
  }

  /**
   * Get timestamp of the start time.
   *
   * @return int
   */
  public function get_start_time() : int {
    return $this->start_time;
  }

  /**
   * Get start time timestamp value.
   *
   * @return int
   */
  protected function get_start_time_value() : int {
    if ($this->start_time > 0) {
      return $this->start_time;
    }
    else {
      return time();
    }
  }

  /**
   * Get start time URL parameter.
   *
   * @return string
   */
  public function get_start_time_field() : string {
    if ($this->start_time > 0 && $this->start_time < 4294967295) {
      return 'st=' . $this->get_start_time_value() . $this->field_delimiter;
    }
    else {
      return '';
    }
  }

  /**
   * Set token validity time window.
   *
   * @param int $window
   *   Token validity time window in seconds.
   */
  public function set_window(int $window) {
    if ($window > 0) {
      $this->window = $window;
    }
    else {
      if ($this->debug === TRUE) {
        $msg = $this->t('Window input invalid.', [], ['context' => 'Wedia']);
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
    }
  }

  /**
   * Get token validity time window.
   *
   * @return int
   */
  public function get_window() : int {
    return $this->window;
  }

  /**
   * Get exp URL parameter.
   *
   * @return string
   */
  public function get_expr_field() : string {
    return 'exp=' . ($this->get_start_time_value() + $this->window) . $this->field_delimiter;
  }

  /**
   * Set ACL.
   *
   * @param string $acl
   *   ACL.
   */
  public function set_acl(string $acl) {
    if (empty($this->url) === FALSE) {
      if ($this->debug === TRUE) {
        $msg = $this->t(
          'Cannot set both an ACL and a URL at the same time.',
          [],
          ['context' => 'Wedia']
        );
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
      return;
    }
    $this->acl = $acl;
  }

  /**
   * Get ACL.
   *
   * @return string
   */
  public function get_acl() : string {
    return $this->acl;
  }

  /**
   * Get ACL URL parameter.
   *
   * @return string
   */
  public function get_acl_field() : string {
    if ($this->acl) {
      return 'acl=' . $this->encode($this->acl) . $this->field_delimiter;
    }
    elseif (empty($this->url) === TRUE) {
      //return a default open acl
      return 'acl=' . $this->encode('/*') . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set URL.
   *
   * @param string $url
   *   URL.
   */
  public function set_url(string $url) {
    if (empty($this->acl) === FALSE) {
      if ($this->debug === TRUE) {
        $msg = $this->t(
          'Cannot set both an ACL and a URL at the same time.',
          [],
          ['context' => 'Wedia']
        );
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
      return;
    }
    $this->url = $url;
  }

  /**
   * Get URL.
   *
   * @return string
   */
  public function get_url() : string {
    return $this->url;
  }

  /**
   * Get URL parameter.
   *
   * @return string
   */
  public function get_url_field() : string {
    if (empty($this->url) === FALSE && empty($this->acl) === TRUE) {
      return 'url=' . $this->encode($this->url) . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set session identifier.
   *
   * @param string $session_id
   *   Session ID.
   */
  public function set_session_id(string $session_id) {
    $this->session_id = $session_id;
  }

  /**
   * Get session identifier.
   *
   * @return string
   */
  public function get_session_id() : string {
    return $this->session_id;
  }

  /**
   * Get session identifier URL parameter.
   *
   * @return string
   */
  public function get_session_id_field() : string {
    if (empty($this->session_id) === FALSE) {
      return 'id=' . $this->session_id . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set data.
   *
   * @param string
   *   Data.
   */
  public function set_data(string $data) {
    $this->data = $data;
  }

  /**
   * Get data.
   *
   * @return string
   */
  public function get_data() : string {
    return $this->data;
  }

  /**
   * Get data URL parameter.
   *
   * @return string
   */
  public function get_data_field() : string {
    if (empty($this->data) === FALSE) {
      return 'data=' . $this->data . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set salt.
   *
   * @param string $salt
   *   Salt.
   */
  public function set_salt(string $salt) {
    $this->salt = $salt;
  }

  /**
   * Get salt.
   *
   * @return string
   */
  public function get_salt() : string {
    return $this->salt;
  }

  /**
   * Get salt URL parameter.
   *
   * @return string
   */
  public function get_salt_field() : string {
    if (empty($this->salt) === FALSE) {
      return 'salt=' . $this->salt . $this->field_delimiter;
    }
    return '';
  }

  /**
   * Set key.
   *
   * @param string $key
   *   Description.
   */
  public function set_key(string $key) {
    // Verify the key is valid hex and has a even number of caracters (64).
    if (
      (bool) preg_match('/^[a-fA-F0-9]+$/', $key) === TRUE
      && (strlen($key) % 2) == 0
    ) {
      $this->key = $key;
    }
    else {
      if ($this->debug === TRUE) {
        $msg = $this->t(
          'Key must be a hex string (a-f, 0-9 and even number of chars).',
          [],
          ['context' => 'Wedia']
        );
        $this->logger->error($msg);
        throw new Akamai_EdgeAuth_ParameterException($msg);
      }
    }
  }

  /**
   * Get key.
   *
   * @return string
   */
  public function get_key() : string {
    return $this->key;
  }

  /**
   * Set field delimiter in URL.
   *
   * @param string $field_delimiter
   *   URL parameter field delimiter.
   */
  public function set_field_delimiter(string $field_delimiter) {
    $this->field_delimiter = $field_delimiter;
  }

  /**
   * Get URL parameter delimiter.
   *
   * @return string
   */
  public function get_field_delimiter() : string {
    return $this->field_delimiter;
  }

  /**
   * Set early URL encoding.
   *
   * @param bool $early_url_encoding
   *   Flag for early URL encoding.
   *
   * @return string
   */
  public function set_early_url_encoding(bool $early_url_encoding) {
    $this->early_url_encoding = $early_url_encoding;
  }

  /**
   * Get early URL encoding.
   *
   * @return bool
   */
  public function get_early_url_encoding() : bool {
    return $this->early_url_encoding;
  }

  /**
   * Set debug.
   *
   * @param bool $debug
   *   Flag for debug enabled.
   */
  public function set_debug(bool $debug) {
    $this->debug = $debug;
  }

  /**
   * Get debug.
   *
   * @return bool
   */
  public function get_debug() : bool {
    return $this->debug;
  }

}
