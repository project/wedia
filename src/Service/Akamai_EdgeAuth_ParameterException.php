<?php

namespace Drupal\wedia\Service;

use \Exception;

/**
 * Akamai token authentification exception.
 */
class Akamai_EdgeAuth_ParameterException extends Exception {
}
