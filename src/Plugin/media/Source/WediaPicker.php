<?php

namespace Drupal\wedia\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;

/**
 * External image entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "wedia_picker",
 *   label = @Translation("Wedia Content Picker"),
 *   description = @Translation("Use remote images."),
 *   allowed_field_types = {"text_long"},
 *   thumbnail_alt_metadata_attribute = "alt",
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class WediaPicker extends MediaSourceBase {

  /**
   * Get media metadata attributes.
   *
   * @return array
   */
  public function getMetadataAttributes() : array {
    return [
      'title' => $this->t('Title', [], ['context' => 'Wedia']),
      'alt_text' => $this->t('Alternative text', [], ['context' => 'Wedia']),
      'caption' => $this->t('Caption', [], ['context' => 'Wedia']),
      'credit' => $this->t('Credit', [], ['context' => 'Wedia']),
      'id' => $this->t('ID', [], ['context' => 'Wedia']),
      'uri' => $this->t('URL', [], ['context' => 'Wedia']),
      'width' => $this->t('Width', [], ['context' => 'Wedia']),
      'height' => $this->t('Height', [], ['context' => 'Wedia']),
    ];
  }

  /**
   * Get media metadata.
   *
   * @param MediaInterface $media
   *   The media interface.
   * @param string $attribute_name
   *   Attribute name.
   *
   * @return mixed|string|null
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    // Get the text_long field where the JSON object is stored
    $remote_field = $media->get($this->configuration['source_field']);
    $json_arr = json_decode($remote_field->value);
    // If the source field is not required, it may be empty.
    if (!$remote_field) {
      return parent::getMetadata($media, $attribute_name);
    }
    switch ($attribute_name) {
      // This is used to set the name of the media entity if the user leaves the field blank.
      case 'default_name':
        return $json_arr->alt_text;
      // This is used to generate the thumbnail field.
      case 'thumbnail_uri':
        return $json_arr->uri;
      default:
        return $json_arr->$attribute_name ?? parent::getMetadata($media, $attribute_name);
    }
  }

}
