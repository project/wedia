<?php

namespace Drupal\wedia\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for wedia image links.
 *
 * @Constraint(
 *   id = "WediaImageLink",
 *   label = @Translation("Link data valid for wedia image.", context = "Validation"),
 * )
 */
class WediaImageLinkConstraint extends Constraint {

  public $message = "The path '@uri' is invalid.";

}
