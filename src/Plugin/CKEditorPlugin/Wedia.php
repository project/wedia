<?php

namespace Drupal\wedia\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\editor\Entity\Editor;
use Drupal\views\ViewExecutable;

/**
 * Defines the "Wedia" plugin, with a CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "wedia",
 *   label = @Translation("Wedia Plugin")
 * )
 */
class Wedia extends PluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return ['wedia/wedia_ck_editor'];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'wedia') . '/js/plugins/wedia/plugin.js';
  }

  /**
   * @return array
   */
  public function getButtons() {
    return [
      'Wedia' => [
        'label' => t('Add Wedia', [], ['context' => 'Wedia']),
        'image' => drupal_get_path('module', 'wedia') . '/js/plugins/wedia/images/logo.png',
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = \Drupal::config('wedia.settings');
    // Include content picker options in Javascript.
    return [
      'lang' => (string) \Drupal::languageManager()->getCurrentLanguage()->getId(),
      'debug' => (int) $config->get('debug'),
      'server_url' => (string) $config->get('server_url'),
      'preview' => (int) $config->get('preview'),
      'filters' => (int) $config->get('filters'),
      'sort' => (int) $config->get('sort'),
      'cursors' => (int) $config->get('cursors'),
      'is_file_custom_name' => (int) $config->get('is_file_custom_name'),
      'file_custom_name_property' => (string) $config->get('file_custom_name_property'),
      'default_variation' => (string) $config->get('default_variation'),
      'ck_editor_options' => (string) $config->get('ck_editor_options'),
      'ck_editor_width' => (string) $config->get('ck_editor_width'),
      'ck_editor_height' => (string) $config->get('ck_editor_height'),
    ];
  }

}
