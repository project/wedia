<?php

namespace Drupal\wedia\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wedia image field formatter.
 *
 * @FieldFormatter(
 *   id = "wedia_image_field_formatter",
 *   label = @Translation("Wedia image formatter"),
 *   description = @Translation("Display the wedia image"),
 *   field_types = {
 *     "wedia_image_field"
 *   }
 * )
 */
class WediaImageFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs an WediaImageFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $base_cache_tags = [];
    $base_cache_context = [];
    $width = NULL;
    $entity = $items->getEntity();
    $base_cache_context[] = 'url.site';

    foreach ($items as $delta => $item) {
      $img = $this->getImgAttributes($item->getValue(), $this->getSetting('image_style'));
      $element[$delta] = [
        '#theme' => 'wedia_image_formatter',
        '#uri' => $img['uri'],
        '#alt' => $img['alt'],
        '#width' => $img['width'],
        '#height' => $img['height'],
        '#cache' => [
          'tags' => Cache::mergeTags($img['cache_tags'], $entity->getCacheTags()),
          'contexts' => Cache::mergeContexts($img['cache_contexts'], $entity->getCacheContexts()),
        ],
      ];
    }

    return $element;
  }

  /**
   * Get image attributes values.
   *
   * @param array $item_values
   *   Entity field item values.
   * @param array $image_style_setting
   *   Image style field setting.
   */
  protected function getImgAttributes($item_values, $image_style_setting = NULL) {
    $attributes = [
      'alt' => $item_values['alt'],
      'uri' => $item_values['uri'],
      'width' => $item_values['width'] ?: NULL,
      'height' => $item_values['height'] ?: NULL,
      'cache_tags' => [],
      'cache_contexts' => ['url.site'],
    ];
    // No image style.
    if (empty($image_style_setting) === TRUE) {
      return $attributes;
    }
    // Load image style.
    $image_style = $this->imageStyleStorage->load($image_style_setting);
    if (empty($image_style) === TRUE) {
      return $attributes;
    }
    // Apply cache tags.
    $attributes['cache_tags'] = $image_style->getCacheTags() ?? [];
    $attributes['cache_contexts'] = array_merge(
      $attributes['cache_contexts'],
      $image_style->getCacheContexts()
    );
    // Get effects.
    $effects = $image_style->getEffects()->getConfiguration();
    if (empty($effects) === TRUE) {
      return $attributes;
    }
    // Apply effects in the URL.
    foreach ($effects as $uuid => $effect) {
      // Check for scale effect.
      if (in_array($effect['id'], ['image_scale', 'image_scale_and_crop', 'image_resize', 'image_crop']) === TRUE) {
        $attributes['width'] = $effect['data']['width'];
        $attributes['height'] = $effect['data']['height'];
        $attributes['uri'] = $this->updateUriParameter($attributes['uri'], 'outputWidth', $effect['data']['width']);
        $attributes['uri'] = $this->updateUriParameter($attributes['uri'], 'outputHeight', $effect['data']['height']);
      }
      // Check for convert effect.
      if ($effect['id'] === 'image_convert') {
        $attributes['uri'] = str_replace('?', '.' . $effect['data']['extension'] . '?', $attributes['uri']);
      }
    }

    return $attributes;
  }

  /**
   * Update an existing GET parameter in a URL, or add it if no exist.
   *
   * @param string $uri
   *   The full absolute URL to update.
   * @param string $parameter
   *   The GET parameter name to update or add.
   * @param string $value
   *   The GET parameter value.
   */
  protected function updateUriParameter($uri, $parameter, $value) {
    $parse_url = parse_url($uri);
    parse_str($parse_url['query'], $parameters);
    $parameters[$parameter] = $value;
    $parse_url['query'] = http_build_query($parameters);
    $new_url = $parse_url['scheme'] . '://' . $parse_url['host'] . $parse_url['path'] . '?' . $parse_url['query'];

    return $new_url;
  }

}
