<?php

namespace Drupal\wedia\Plugin\Field\FieldFormatter;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wedia\Helper\DntHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wedia asset field formatter.
 *
 * @FieldFormatter(
 *   id = "wedia_asset_field_formatter",
 *   label = @Translation("Wedia asset formatter"),
 *   description = @Translation("Display the wedia asset"),
 *   field_types = {
 *     "wedia_asset_field"
 *   }
 * )
 */
class WediaAssetFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The Wedia DNT helper.
   *
   * @var \Drupal\wedia\Helper\DntHelper
   */
  protected $dnt;

  /**
   * Constructs an WediaAssetFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration factory.
   * @param \Drupal\wedia\Helper\DntHelper $dnt
   *   The Wedia DNT helper.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactory $config, DntHelper $dnt) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->config = $config;
    $this->dnt = $dnt;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('wedia.dnt_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $code_embed = $values['code_embed'];
      $type = $values['type'];
      // Add DNT parameter inside video embed HTML code.
      if ($type == 'video') {
        $dnt = (bool) $this->config->get('wedia.settings')->get('dnt');
        if ($dnt === TRUE) {
          $code_embed = $this->dnt->addToHtml($code_embed);
        }
      }
      $element[$delta] = [
        '#theme' => 'wedia_asset_formatter',
        '#type' => $type,
        '#uri' => $values['uri'],
        '#alt' => $values['alt'],
        '#width' => $values['width'],
        '#height' => $values['height'],
        '#code_embed' => $code_embed,
        '#cache' => [
          'tags' => $entity->getCacheTags(),
          'contexts' => ['url.site'],
        ],
      ];
    }

    return $element;
  }

}
