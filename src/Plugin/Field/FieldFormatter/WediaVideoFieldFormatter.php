<?php

namespace Drupal\wedia\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wedia\Helper\DntHelper;
use Drupal\wedia\Helper\TokenHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wedia video field formatter.
 *
 * @FieldFormatter(
 *   id = "wedia_video_field_formatter",
 *   label = @Translation("Wedia video formatter"),
 *   description = @Translation("Display the wedia video"),
 *   field_types = {
 *     "wedia_video_field"
 *   }
 * )
 */
class WediaVideoFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The Wedia DNT helper.
   *
   * @var \Drupal\wedia\Helper\DntHelper
   */
  protected $dnt;

  /**
   * The Wedia token helper.
   *
   * @var \Drupal\wedia\Helper\TokenHelper
   */
  protected $token;

  /**
   * Constructs an WediaVideoFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration factory.
   * @param \Drupal\wedia\Helper\DntHelper $dnt
   *   The Wedia DNT helper.
   * @param \Drupal\wedia\Helper\TokenHelper $token
   *   The Wedia token helper.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactory $config, DntHelper $dnt, TokenHelper $token) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->config = $config;
    $this->dnt = $dnt;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('wedia.dnt_helper'),
      $container->get('wedia.token_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $config = $this->config->get('wedia.settings');
    $dnt = (bool) $config->get('dnt');
    $token = (bool) $config->get('token');

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $html = $values['html'];
      if ($dnt === TRUE) {
        $html = $this->dnt->addToHtml($html);
      }
      if ($token === TRUE) {
        $html = $this->token->addToHtml($html);
      }
      $element[$delta] = [
        '#theme' => 'wedia_video_formatter',
        '#html' => $html,
        '#width' => $values['width'],
        '#height' => $values['height'],
        '#cache' => [
          'tags' => $entity->getCacheTags(),
          'contexts' => ['url.site'],
        ],
      ];
    }

    return $element;
  }

}
