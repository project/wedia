<?php

namespace Drupal\wedia\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Core\Render\Markup;

/**
 * Wedia image field widget.
 *
 * @FieldWidget(
 *   id = "wedia_image_field_widget",
 *   label = @Translation("Wedia image picker"),
 *   description = @Translation("Button for open the Wedia content picker and alternative text field."),
 *   field_types = {
 *     "wedia_image_field"
 *   }
 * )
 */
class WediaImageFieldWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $config = \Drupal::config('wedia.settings');
    $item = $items[$delta];
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $element['preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'wedia-preview',
      ],
    ];
    if (empty($item->uri) === FALSE) {
      $element['preview']['img'] = [
        '#theme' => 'image',
        '#uri' => $item->uri,
        '#alt' => '',
        '#title' => '',
        '#attributes' => [
          'class' => ['wedia-preview-img'],
        ],
      ];
      $element['clear'] = [
        '#type' => 'button',
        '#value' => $this->t('Clear', [], ['context' => 'Wedia']),
        '#attributes' => [
          'class' => ['wedia-img-clear-action', 'btn', 'btn-primary'],
        ],
      ];
    }
    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternative text', [], ['context' => 'Wedia']),
      '#default_value' => isset($item->alt) ? $item->alt : NULL,
      '#maxlength' => 255,
      '#description' => $this->t('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.', [], ['context' => 'Wedia']),
      '#attributes' => [
        'class' => ['wedia-img-alt'],
      ],
    ];
    $element['options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Content picker options', [], ['context' => 'Wedia']),
      '#options' => [
        'default' => $this->t('Let the template manage the image dimension.', [], ['context' => 'Wedia']),
        'focus' => $this->t('Let the template manage the image dimension, but add of the focus point.', [], ['context' => 'Wedia']),
        'crop' => $this->t('Constrain the image dimensions (crop) :', [], ['context' => 'Wedia']),
      ],
      '#default_value' => 'default',
      '#attributes' => [
        'class' => ['wedia-img-options'],
      ],
    ];
    $element['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Image width in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => (empty($item->width) === FALSE) ? $item->width : '',
      '#attributes' => [
        'class' => ['wedia-img-width'],
        'type' => 'number',
      ],
    ];
    $element['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heigth', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Image height in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => (empty($item->height) === FALSE) ? $item->height : '',
      '#attributes' => [
        'class' => ['wedia-img-height'],
        'type' => 'number',
      ],
    ];
    $element['open'] = [
      '#type' => 'button',
      '#value' => $this->t('Select image in the Wedia DAM', [], ['context' => 'Wedia']),
      '#attributes' => [
        'class' => ['wedia-img-insert-button', 'btn', 'btn-primary'],
      ],
      '#attached' => [
        'library' => [
          'wedia/wedia_image_field_widget',
        ],
        'drupalSettings' => [
          'Wedia' =>
          [
            'lang' => (string) $lang,
            'server_url' => $config->get('server_url'),
            'preview' => $config->get('preview'),
            'filters' => $config->get('filters'),
            'sort' => $config->get('sort'),
            'cursors' => $config->get('cursors'),
            'is_file_custom_name' => $config->get('is_file_custom_name'),
            'file_custom_name_property' => $config->get('file_custom_name_property'),
            'file_resolution' => $config->get('file_resolution'),
            'variation' => $config->get('default_variation'),
            'debug' => $config->get('debug'),
          ],
        ],
      ],
      '#limit_validation_errors' => [],
    ];
    $element['uri'] = [
      '#type' => 'url',
      '#default_value' => $item->uri,
      '#element_validate' => [[get_called_class(), 'validateUriElement']],
      '#maxlength' => 2048,
      '#attributes' => [
        'class' => ['wedia-img-uri', 'hidden'],
      ],
    ];
    $element['wedia_content_picker'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['wedia-content-picker'],
      ],
      'header' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-header'],
        ],
        'close' => [
          '#type' => 'markup',
          '#markup' => Markup::create('<div class="wedia-content-picker-close">X</div>'),
        ],
      ],
      'iframe' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-iframe'],
        ],
      ],
    ];
    $element += [
      '#type' => 'fieldset',
    ];

    return $element;
  }

  /**
   * Form element validation handler for the 'uri' element.
   * Disallows saving inaccessible or untrusted URLs.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form.
   *
   * @return void
   */
  public static function validateUriElement(
    $element,
    FormStateInterface $form_state,
    $form
  ) {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);
    if (
      isset($element['#required']) === TRUE
      && $element['#required'] === TRUE
      && empty($uri) === TRUE
    ) {
      $form_state->setError(
        $element,
        new TranslatableMarkup(
          'Field @field_name is required.',
          ['@field_name' => ''],
          ['context' => 'Wedia']
        )
      );
      return;
    }
  }

}
