<?php

namespace Drupal\wedia\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Render\Markup;

/**
 * Wedia asset field widget.
 *
 * @FieldWidget(
 *   id = "wedia_asset_field_widget",
 *   label = @Translation("Wedia asset picker"),
 *   description = @Translation("Button for open the Wedia content picker."),
 *   field_types = {
 *     "wedia_asset_field"
 *   }
 * )
 */
class WediaAssetFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $config = \Drupal::config('wedia.settings');
    $item = $items[$delta];
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $is_empty = TRUE;
    $element['wedia_asset_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'wedia-asset-preview',
      ],
    ];
    if ($item->type != 'photo' && empty($item->code_embed) === FALSE) {
      $is_empty = FALSE;
      $element['wedia_asset_preview']['iframe'] = [
        '#type' => 'item',
        '#markup' => Markup::create($item->code_embed),
      ];
    }
    if ($item->type == 'photo' && empty($item->uri) === FALSE) {
      $is_empty = FALSE;
      $element['wedia_asset_preview']['image'] = [
        '#theme' => 'image',
        '#uri' => $item->uri,
        '#alt' => '',
        '#title' => '',
        '#attributes' => [
          'class' => ['wedia-image-preview'],
        ],
      ];
    }
    if ($is_empty === FALSE) {
      $element['clear'] = [
        '#type' => 'button',
        '#value' => $this->t('Clear', [], ['context' => 'Wedia']),
        '#attributes' => [
          'class' => ['wedia-asset-clear-action', 'btn', 'btn-primary'],
        ],
      ];
    }
    $element['code_embed'] = [
      '#type' => 'textarea',
      '#default_value' => isset($item->code_embed) ? $item->code_embed : '',
      '#attributes' => [
        'class' => ['wedia-asset-code-embed', 'hidden'],
      ],
    ];
    $element['uri'] = [
      '#type' => 'textfield',
      '#default_value' => isset($item->uri) ? $item->uri : '',
      '#attributes' => [
        'class' => ['wedia-asset-uri', 'hidden'],
      ],
      '#maxlength' => 2048,
    ];
    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image alternative text', [], ['context' => 'Wedia']),
      '#default_value' => isset($item->alt) ? $item->alt : '',
      '#maxlength' => 255,
      '#description' => $this->t('Short description of the asset used by screen readers and displayed when the asset is not loaded. This is important for accessibility.', [], ['context' => 'Wedia']),
      '#attributes' => [
        'class' => ['wedia-asset-alt'],
      ],
    ];
    $element['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Asset size', [], ['context' => 'Wedia']),
      '#options' => [
        '' => $this->t('-- Select a video size --', [], ['context' => 'Wedia']),
        '510_288' => '510 x 288',
        '640_360' => '640 x 360',
        '768_432' => '768 x 432',
        '896_504' => '896 x 504',
        '1024_576' => '1024 x 576',
        '1152_648' => '1152 x 648',
        '1280_720' => '1280 x 720',
        '1408_792' => '1408 x 792',
        'custom' => $this->t('Custom size', [], ['context' => 'Wedia']),
      ],
      '#default_value' => (empty($item->size) === FALSE) ? $item->size : '',
      '#attributes' => [
        'class' => ['wedia-asset-video-size'],
        
      ],
    ];
    $element['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width', [], ['context' => 'Wedia']),
      '#description' => $this->t('Iframe asset width in pixels.', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#default_value' => (empty($item->width) === FALSE) ? $item->width : '',
      '#attributes' => [
        'class' => ['wedia-asset-width'],
        'type' => 'number',
      ],
    ];
    $element['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height', [], ['context' => 'Wedia']),
      '#description' => $this->t('Iframe asset height in pixels.', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#default_value' => (empty($item->height) === FALSE) ? $item->height : '',
      '#attributes' => [
        'class' => ['wedia-asset-height'],
        'type' => 'number',
      ],
    ];
    $element['type'] = [
      '#type' => 'textfield',
      '#default_value' => isset($item->type) ? $item->type : NULL,
      '#maxlength' => 255,
      '#attributes' => [
        'class' => ['wedia-asset-type', 'hidden'],
      ],
    ];
    $element['open'] = [
      '#type' => 'button',
      '#value' => $this->t('Select asset in the Wedia DAM', [], ['context' => 'Wedia']),
      '#attributes' => [
        'class' => ['wedia-asset-insert-button', 'btn', 'btn-primary'],
      ],
      '#attached' => [
        'library' => [
          'wedia/wedia_asset_field_widget',
        ],
        'drupalSettings' => [
          'Wedia' =>
          [
            'lang' => (string) $lang,
            'server_url' => $config->get('server_url'),
            'preview' => $config->get('preview'),
            'filters' => $config->get('filters'),
            'sort' => $config->get('sort'),
            'cursors' => $config->get('cursors'),
            'is_file_custom_name' => $config->get('is_file_custom_name'),
            'file_custom_name_property' => $config->get('file_custom_name_property'),
            'file_resolution' => $config->get('file_resolution'),
            'variation' => $config->get('default_variation'),
            'debug' => $config->get('debug'),
            'dnt' => $config->get('dnt'),
          ],
        ],
      ],
      '#limit_validation_errors' => [],
    ];
    $element['wedia_content_picker'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['wedia-content-picker'],
      ],
      'header' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-header'],
        ],
        'close' => [
          '#type' => 'markup',
          '#markup' => Markup::create('<div class="wedia-content-picker-close">X</div>'),
        ],
      ],
      'iframe' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-iframe'],
        ],
      ],
    ];
    $element += [
      '#type' => 'fieldset',
    ];

    return $element;
  }

}
