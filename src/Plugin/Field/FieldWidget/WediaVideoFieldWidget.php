<?php

namespace Drupal\wedia\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Markup;

/**
 * Wedia video field widget.
 *
 * @FieldWidget(
 *   id = "wedia_video_field_widget",
 *   label = @Translation("Wedia video picker"),
 *   description = @Translation("Button for open the Wedia content picker and video size configration."),
 *   field_types = {
 *     "wedia_video_field"
 *   }
 * )
 */
class WediaVideoFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $config = \Drupal::config('wedia.settings');
    $item = $items[$delta];
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $element['html'] = [
      '#type' => 'textarea',
      '#default_value' => $item->html,
      '#attributes' => [
        'class' => ['wedia-video-html'],
      ],
    ];
    $element['preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['wedia-video-preview'],
      ],
    ];
    if (empty($item->html) === FALSE) {
      $element['preview']['iframe'] = [
        '#type' => 'item',
        '#markup' => Markup::create($item->html),
      ];
      $element['clear'] = [
        '#type' => 'button',
        '#value' => $this->t('Clear', [], ['context' => 'Wedia']),
        '#attributes' => [
          'class' => ['wedia-video-clear-action', 'btn', 'btn-primary'],
        ],
      ];
    }
    else {
      $element['preview']['#attributes']['class'][] = 'hidden';
    }
    $element['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Video size', [], ['context' => 'Wedia']),
      '#options' => [
        '' => $this->t('-- Select a video size --', [], ['context' => 'Wedia']),
        '510_288' => '510 x 288',
        '640_360' => '640 x 360',
        '768_432' => '768 x 432',
        '896_504' => '896 x 504',
        '1024_576' => '1024 x 576',
        '1152_648' => '1152 x 648',
        '1280_720' => '1280 x 720',
        '1408_792' => '1408 x 792',
        'custom' => $this->t('Custom size', [], ['context' => 'Wedia']),
      ],
      '#default_value' => (empty($item->size) === FALSE) ? $item->size : '',
      '#attributes' => [
        'class' => ['wedia-video-size'],
      ],
    ];
    $element['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Iframe video width in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => (empty($item->width) === FALSE) ? $item->width : '560',
      '#attributes' => [
        'class' => ['wedia-size-custom', 'wedia-video-width'],
        'type' => 'number',
      ],
    ];
    $element['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heigth', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Iframe video height in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => (empty($item->height) === FALSE) ? $item->height : '315',
      '#attributes' => [
        'class' => ['wedia-size-custom', 'wedia-video-height'],
        'type' => 'number',
      ],
    ];
    $element['open'] = [
      '#type' => 'button',
      '#value' => $this->t('Select video in the Wedia DAM', [], ['context' => 'Wedia']),
      '#attributes' => [
        'class' => ['wedia-video-insert-button', 'btn', 'btn-primary'],
      ],
      '#attached' => [
        'library' => [
          'wedia/wedia_video_field_widget',
        ],
        'drupalSettings' => [
          'Wedia' =>
          [
            'lang' => (string) $lang,
            'server_url' => $config->get('server_url'),
            'preview' => $config->get('preview'),
            'filters' => $config->get('filters'),
            'sort' => $config->get('sort'),
            'cursors' => $config->get('cursors'),
            'debug' => $config->get('debug'),
          ],
        ],
      ],
      '#limit_validation_errors' => [],
    ];
    $element['wedia_content_picker'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['wedia-content-picker'],
      ],
      'header' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-header'],
        ],
        'close' => [
          '#type' => 'markup',
          '#markup' => Markup::create('<div class="wedia-content-picker-close">X</div>'),
        ],
      ],
      'iframe' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['wedia-content-picker-iframe'],
        ],
      ],
    ];
    $element += [
      '#type' => 'fieldset',
    ];

    return $element;
  }

}
