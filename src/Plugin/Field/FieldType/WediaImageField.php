<?php

namespace Drupal\wedia\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'wedia_image_field' field type.
 *
 * @FieldType(
 *   id = "wedia_image_field",
 *   label = @Translation("Wedia Image"),
 *   description = @Translation("This field is used for wedia image integration."),
 *   category = @Translation("Reference"),
 *   default_widget = "wedia_image_field_widget",
 *   default_formatter = "wedia_image_field_formatter",
 *   constraints = {"WediaImageLink" = {}}
 * )
 */
class WediaImageField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Image URI.
    $properties['uri'] = DataDefinition::create('uri')
      ->setLabel(
        new TranslatableMarkup('URI', [], ['context' => 'Wedia'])
      )
      ->setDescription(
        new TranslatableMarkup(
          'Image absolute URI used for the source attribute of the img markup.',
          [],
          ['context' => 'Wedia']
        )
      );
    // Image alternative text.
    $properties['alt'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup('Alternative text', [], ['context' => 'Wedia'])
      )
      ->setDescription(
        new TranslatableMarkup(
          "Alternative image text, for the image's 'alt' attribute.",
          [],
          ['context' => 'Wedia']
        )
      );
    // Image width.
    $properties['width'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Width', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Image width.", [], ['context' => 'Wedia']
        )
      );
    // Image height.
    $properties['height'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Height', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Image height.", [], ['context' => 'Wedia']
        )
      );

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'uri' => [
          'description' => 'Image URI.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'alt' => [
          'description' => 'Image alternative text.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'width' => [
          'description' => 'Image width.',
          'type' => 'int',
          'size' => 'normal',
          'unsigned' => TRUE,
        ],
        'height' => [
          'description' => 'Image height.',
          'type' => 'int',
          'size' => 'normal',
          'unsigned' => TRUE,
        ],
      ],
      'indexes' => [
        'uri' => [['uri', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('uri')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'uri';
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return $this->getUrl()->isExternal();
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return Url::fromUri($this->uri);
  }

}
