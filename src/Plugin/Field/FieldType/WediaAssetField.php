<?php

namespace Drupal\wedia\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'wedia_image_field' field type.
 *
 * @FieldType(
 *   id = "wedia_asset_field",
 *   label = @Translation("Wedia Asset"),
 *   description = @Translation("This field is used for wedia generic asset."),
 *   category = @Translation("Reference"),
 *   default_widget = "wedia_asset_field_widget",
 *   default_formatter = "wedia_asset_field_formatter"
 * )
 */
class WediaAssetField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Asset type.
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup('Type', [], ['context' => 'Wedia'])
      )
      ->setDescription(
        new TranslatableMarkup('Asset type.', [], ['context' => 'Wedia'])
      );
    // Image URI.
    $properties['uri'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup('URL', [], ['context' => 'Wedia'])
      )
      ->setDescription(
        new TranslatableMarkup(
          'Image source absolute URL.', [], ['context' => 'Wedia']
        )
      );
    // Image alternative text.
    $properties['alt'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup('Alternative text', [], ['context' => 'Wedia'])
      )
      ->setDescription(
        new TranslatableMarkup(
          "Alternative image text, for the image's 'alt' attribute.",
          [],
          ['context' => 'Wedia']
        )
      );
    // Image width.
    $properties['width'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Width', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Image width in pixel.", [], ['context' => 'Wedia']
        )
      );
    // Image height.
    $properties['height'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Height', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Image height in pixel.", [], ['context' => 'Wedia']
        )
      );
    // Video size.
    $properties['size'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup(
          'Video size', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          'Video preset width and height or custom.', [], ['context' => 'Wedia']
        )
      );
    // Code embed for other asset type than image.
    $properties['code_embed'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup(
          'Code embed.', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Code embed of the asset.", [], ['context' => 'Wedia']
        )
      );

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'type' => [
          'description' => 'Asset type.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'uri' => [
          'description' => 'Image URI.',
          'type' => 'varchar',
          'length' => 2048,
          'not null' => FALSE,
        ],
        'alt' => [
          'description' => 'Image alternative text.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        'width' => [
          'description' => 'Image width.',
          'type' => 'int',
          'size' => 'normal',
          'not null' => FALSE,
        ],
        'height' => [
          'description' => 'Image height.',
          'type' => 'int',
          'size' => 'normal',
          'not null' => FALSE,
        ],
        'size' => [
          'description' => 'Video size.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        'code_embed' => [
          'description' => 'Code embed.',
          'type' => 'text',
          'size' => 'medium',
          'not null' => FALSE,
        ],
      ],
      'indexes' => [
        'type' => [['type', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $type = $this->get('type')->getValue();
    switch ($type) {
      case 'video':
        return empty($this->get('code_embed')->getValue());
        break;
      case 'photo':
        return empty($this->get('uri')->getValue());
        break;
      default:
        return empty($this->get('code_embed')->getValue());
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'type';
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return Url::fromUri($this->uri);
  }

  /**
   * Asset type.
   *
   * @return string
   */
  public function getAssetType() {
    return $this->get('type')->getValue();
  }

}
