<?php

namespace Drupal\wedia\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'wedia_video_field' field type.
 *
 * @FieldType(
 *   id = "wedia_video_field",
 *   label = @Translation("Wedia Video"),
 *   description = @Translation("This field is used for wedia video integration."),
 *   category = @Translation("Reference"),
 *   default_widget = "wedia_video_field_widget",
 *   default_formatter = "wedia_video_field_formatter"
 * )
 */
class WediaVideoField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['html'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup(
          'Embed HTML code', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          'Embed ready iframe HTML markup with video URL and all preset attributes.', [], ['context' => 'Wedia']
        )
      );
    $properties['size'] = DataDefinition::create('string')
      ->setLabel(
        new TranslatableMarkup(
          'Video size', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          'Video size with preset width and height or custom values.', [], ['context' => 'Wedia']
        )
      );
    $properties['width'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Width', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Iframe video width.", [], ['context' => 'Wedia']
        )
      );
    $properties['height'] = DataDefinition::create('integer')
      ->setLabel(
        new TranslatableMarkup(
          'Height', [], ['context' => 'Wedia']
        )
      )
      ->setDescription(
        new TranslatableMarkup(
          "Iframe video height.", [], ['context' => 'Wedia']
        )
      );

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'html' => [
          'description' => 'Embed HTML code.',
          'type' => 'text',
          'size' => 'medium',
        ],
        'size' => [
          'description' => 'The video size.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'width' => [
          'description' => 'Width.',
          'type' => 'int',
          'size' => 'normal',
        ],
        'height' => [
          'description' => 'Height.',
          'type' => 'int',
          'size' => 'normal',
        ],
      ],
      'indexes' => [
        'html' => [['html', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('html')->getValue();
    return empty($value);
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'html';
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return TRUE;
  }

  /**
   * The video has a custom size.
   *
   * @return bool
   */
  public function hasCustomSize() {
    return ($this->get('size')->getValue() == 'custom') ? TRUE : FALSE;
  }

}
