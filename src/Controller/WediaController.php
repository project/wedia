<?php

namespace Drupal\wedia\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for content picker.
 */
class WediaController extends ControllerBase {

  /**
   * Content picker.
   *
   * @return array
   */
  public function content() : array {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Wedia Settings', [], ['context' => 'Wedia']),
    ];
  }

}
