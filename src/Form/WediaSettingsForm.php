<?php

namespace Drupal\wedia\Form;

use DateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wedia DAM integration settings.
 */
class WediaSettingsForm extends ConfigFormBase {

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Messenger interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ClientInterface $client,
    LoggerChannelFactoryInterface $loggerFactory,
    MessengerInterface $messenger
  ) {
    $this->client = $client;
    $this->loggerFactory = $loggerFactory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wedia_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wedia.settings');
    // Wedia server configuration.
    $form['server'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Wedia Server', [], ['context' => 'Wedia']),
    ];
    $form['server']['server_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('URL', [], ['context' => 'Wedia']),
      '#description' => $this->t('This is the URL of your the Wedia server for your account.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('server_url'),
    ];
    $form['server']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode', [], ['context' => 'Wedia']),
      '#description' => $this->t('This option enable Javascript log in the web browser console. It also enable Drupal log in the database logger if the dblog module is installed and enabled.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('debug') ?? FALSE,
    ];
    // Content picker options.
    $form['content_picker'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content picker', [], ['context' => 'Wedia']),
    ];
    $form['content_picker']['preview'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show asset info popin', [], ['context' => 'Wedia']),
      '#description' => $this->t('This option allows to display or not in the content picker the information popin activated by the "eye" button on the media.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('preview') ?? TRUE,
    ];
    $form['content_picker']['filters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show filters', [], ['context' => 'Wedia']),
      '#description' => $this->t('This option allows to display or not in the content picker the filters on the media list.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('filters') ?? TRUE,
    ];
    $form['content_picker']['cursors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show universes / cursors', [], ['context' => 'Wedia']),
      '#description' => $this->t('This option allows you to display or not in the content picker the list of universe drop-downs or cursors.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('cursors') ?? TRUE,
    ];
    $form['content_picker']['sort'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show sort', [], ['context' => 'Wedia']),
      '#description' => $this->t('This option allows you to display or not the media sorting in the content picker.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('sort') ?? TRUE,
    ];
    $form['content_picker']['dnt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('DNT', [], ['context' => 'Wedia']),
      '#description' => $this->t('Setting this parameter to "true" will block the Wedia player from tracking any session data, including all cookies and analytics.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('dnt') ?? TRUE,
    ];
    $form['content_picker']['token'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Token enabled', [], ['context' => 'Wedia']),
      '#description' => $this->t('Setting this parameter to "true" will add token authentification to all Wedia assets on our website.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('token') ?? FALSE,
    ];
    $form['content_picker']['token_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token secret', [], ['context' => 'Wedia']),
      '#description' => $this->t('The token you got from your Wedia Account Manager.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('token_key') ?? '',
    ];
    $form['content_picker']['token_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token time window', [], ['context' => 'Wedia']),
      '#description' => $this->t('The time window, in seconds, of the token validity.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('token_time') ?? '',
    ];
    // CK Editor options.
    $form['ck_editor'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CK Editor plugin', [], ['context' => 'Wedia']),
    ];
    $form['ck_editor']['ck_editor_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Content picker options', [], ['context' => 'Wedia']),
      '#options' => [
        'default' => $this->t('Let the template manage the image dimension.', [], ['context' => 'Wedia']),
        'focus' => $this->t('Let the template manage the image dimension, but add of the focus point.', [], ['context' => 'Wedia']),
        'crop' => $this->t('Constrain the image dimensions (crop) :', [], ['context' => 'Wedia']),
      ],
      '#default_value' => $config->get('ck_editor_options') ?? 'default',
      '#attributes' => [
        'class' => ['wedia-img-options'],
      ],
    ];
    $form['ck_editor']['ck_editor_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Image width in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('ck_editor_width'),
      '#attributes' => [
        'class' => ['wedia-img-custom-width'],
        'type' => 'number',
      ],
    ];
    $form['ck_editor']['ck_editor_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heigth', [], ['context' => 'Wedia']),
      '#maxlength' => 5,
      '#description' => $this->t('Image height in pixels.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('ck_editor_height'),
      '#attributes' => [
        'class' => ['wedia-img-custom-height'],
        'type' => 'number',
      ],
    ];
    // Media file configuration.
    $form['file'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Image file', [], ['context' => 'Wedia']),
    ]; 
    $form['file']['is_file_custom_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable keywords in file name', [], ['context' => 'Wedia']),
      '#description' => $this->t('Add the values of a custom property returned by the content picker to the generated file name.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('is_file_custom_name') ?? TRUE,
    ];
    $form['file']['file_custom_name_property'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Property to use for the filename', [], ['context' => 'Wedia']),
      '#description' => $this->t('Specify the metadata property to be used.', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('file_custom_name_property') ?? 'keywords',
    ];
    $form['file']['default_variation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default image variation name', [], ['context' => 'Wedia']),
      '#description' => $this->t('Specify the image variation name of the returned image ("thumbnailScreen" for example).', [], ['context' => 'Wedia']),
      '#default_value' => $config->get('default_variation'),
    ];
    // Actions.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration', [], ['context' => 'Wedia']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Psr\Log\LoggerInterface $logger */
    $logger = $this->loggerFactory->get('wedia');
    // Default validation.
    parent::validateForm($form, $form_state);
    // Is debug mode enabled.
    $debug = ($form_state->getValue('debug') == TRUE);
    // URL format validation.
    $url = rtrim($form_state->getValue('server_url'), '/') . '/asset-picker/wedia-content-picker.js';
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName(
        'server_url',
        $this->t(
          'The Wedia server URL format is incorrect.',
          [],
          ['context' => 'Wedia']
        )
      );
      return;
    }
    // URL access validation.
    try {
      $response = $this->client->get($url);
    }
    catch (RequestException $e) {
      // HTTP response code 404.
      if ($e->hasResponse() && $e->getResponse()->getStatusCode() == '404') {
        $message = $this->t(
          'The Wedia module could not locate the Wedia Content Picker script, please contact your Wedia account manager to activate this feature.',
          [],
          ['context' => 'Wedia']
        );
        $form_state->setErrorByName('server_url', $message);
        if ($debug === TRUE) {
          $logger->error($message);
        }
        return;
      }
      // HTTP response code other than 404.
      else {
        $error_message = '';
        if ($debug === TRUE) {
          $error_message = $e->getMessage();
          $logger->error($e->getMessage());
        }
        $message = $this->t(
          'The URL is invalid. @exception_message', 
          ['@exception_message' => $error_message],
          ['context' => 'Wedia']
        );
        $form_state->setErrorByName('server_url', $message);
      }
    }
    // Token validation.
    $token_enabled = FALSE;
    $token_error = FALSE;
    $token_time = $form_state->getValue('token_time');
    if ((bool) $form_state->getValue('token') === TRUE) {
      $token_enabled = TRUE;
      // Secret token required.
      if (empty($form_state->getValue('token_key')) === TRUE) {
        $token_enabled = FALSE;
        $token_error = TRUE;
        $form_state->setErrorByName('token_key', $this->t(
          'To enable the token security you must set a token secret.',
          [],
          ['context' => 'Wedia']
        ));
      }
      // Time window token required.
      if (empty($token_time) === TRUE) {
        $token_enabled = FALSE;
        $token_error = TRUE;
        $form_state->setErrorByName('token_time', $this->t(
          'To enable the token security you must set a token time window.',
          [],
          ['context' => 'Wedia']
        ));
      }
      else {
        if (is_numeric($token_time) === FALSE) {
          $token_enabled = FALSE;
          $token_error = TRUE;
          $form_state->setErrorByName('token_time', $this->t(
            'The token time window must be a muber.',
            [],
            ['context' => 'Wedia']
          ));
        }
      }
      if ($token_error === TRUE) {
        return;
      }
    }
    // Token enabled informations.
    if ($token_enabled === TRUE) {
      $start = new DateTime();
      $end = clone $start;
      $end = $end->modify('+ ' . $token_time . ' seconds');
      $this->messenger->addWarning($this->t(
        'Attention, you have just activated the security of videos by token authentication. All Wedia videos on your website are only available for @time seconds from now on. Available period starts at @start and end at @end.',
        [
          '@time' => $token_time,
          '@start' => $start->format('H:i:s'),
          '@end' => ($end instanceof DateTime) ? $end->format('H:i:s') : 'invalid date',
        ],
        ['context' => 'Wedia']
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('wedia.settings');
    $config->set('server_url', (string) $form_state->getValue('server_url'));
    $config->set('debug', (bool) $form_state->getValue('debug'));
    $config->set('preview', (bool) $form_state->getValue('preview'));
    $config->set('filters', (bool) $form_state->getValue('filters'));
    $config->set('cursors', (bool) $form_state->getValue('cursors'));
    $config->set('sort', (bool) $form_state->getValue('sort'));
    $config->set('dnt', (bool) $form_state->getValue('dnt'));
    $config->set('token', (bool) $form_state->getValue('token'));
    $config->set('token_key', (string) $form_state->getValue('token_key'));
    $config->set('token_time', (string) $form_state->getValue('token_time'));
    $config->set('is_file_custom_name', (bool) $form_state->getValue('is_file_custom_name'));
    $config->set('file_custom_name_property', (string) $form_state->getValue('file_custom_name_property'));
    $config->set('default_variation', (string) $form_state->getValue('default_variation'));
    $config->set('ck_editor_options', (string) $form_state->getValue('ck_editor_options'));
    $config->set('ck_editor_width', (string) $form_state->getValue('ck_editor_width'));
    $config->set('ck_editor_height', (string) $form_state->getValue('ck_editor_height'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wedia.settings',
    ];
  }

}
