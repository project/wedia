<?php

namespace Drupal\wedia\Helper;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wedia\Service\Akamai_EdgeAuth_Config;
use Drupal\wedia\Service\Akamai_EdgeAuth_Generate;
use Drupal\wedia\Service\Akamai_EdgeAuth_ParameterException;

/**
 * Helper to generate and validate token.
 */
class TokenHelper {

  use StringTranslationTrait;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger_factory;

  /**
   * The Akamai edge authentificator configuration service.
   *
   * @var \Drupal\wedia\Service\Akamai_EdgeAuth_Config
   */
  protected $akamai_config;

  /**
   * The Akamai edge authentificator generator service.
   *
   * @var \Drupal\wedia\Service\Akamai_EdgeAuth_Generate
   */
  protected $akamai_generate;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactory $config,
    LoggerChannelFactoryInterface $logger_factory,
    Akamai_EdgeAuth_Config $akamai_config,
    Akamai_EdgeAuth_Generate $akamai_generate
  ) {
    $this->config = $config;
    $this->logger = $logger_factory->get('wedia');
    $this->akamai_config = $akamai_config;
    $this->akamai_generate = $akamai_generate;
  }

  /**
   * Add "token" URL parameter in iframe src attribute inside the html code.
   *
   * @param string $html
   *   The code embed to update.
   *
   * @return string
   */
  public function addToHtml(string $html) : string {
    // Check that the HTML is an iframe.
    if (strpos($html, 'iframe') === FALSE) {
      $this->logger->warning($this->t(
        'Try to add token to code embed that is not a iframe.',
        [],
        ['context' => 'Wedia']
      ));
      return $url;
    }

    $html = str_replace(['<iframe ', '></iframe>'], '', $html);
    $attributes = explode(' ', $html);

    foreach ($attributes as $key => $attr) {
      if (substr($attr, 0, 4) == 'src=') {
        $url = $attributes[$key];
        // Remove [src="] at the beggining of the string.
        $url = substr($url, 5);
        // Remove ["] at the end of the string.
        $url = substr_replace($url, '', -1);
        // Now that string is a clean URL, add the token to it.
        $url = $this->addToUrl($url);
        // Rebuild HTML attribute "src".
        $attributes[$key] = 'src="' . $url . '"';
      }
    }

    $new_html = implode(' ', $attributes);
    $new_html = '<iframe ' . $new_html . '></iframe>';

    return $new_html;
  }

  /**
   * Add Wedia secure token to a URL.
   *
   * @param string $url
   *   The URL where the token will be added.
   *
   * @return string
   */
  public function addToUrl(string $url) : string {
    /** @var \Drupal\Core\Config\ImmutableConfig */
    $config = $this->config->get('wedia.settings');

    if ((bool) $config->get('token') === FALSE) {
      $this->logger->warning($this->t(
        'Try to add a token to a URL but the token config is disabled.',
        [],
        ['context' => 'Wedia']
      ));
      return $url;
    }
    else {
      return $this->tokenizeUrl($url, $config);
    }

    return $token;
  }

  /**
   * Generate token and add it to the asset URL.
   *
   * @param string $url
   *   The URL where the token will be added.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The wedia.settings configuration object.
   *
   * @return string
   */
  public function tokenizeUrl(string $url, ImmutableConfig $config) : string {
    // Check that token key exists.
    $key = $config->get('token_key');
    if (empty($key) === TRUE) {
      $this->logger->warning($this->t(
        'Cannot tokenize URL because there is no token secret key set in config.',
        [],
        ['context' => 'Wedia']
      ));
      return $url;
    }
    $timestamp = time();
    $tokenParameterName = 'RequestToken';
    $path = parse_url($url, PHP_URL_PATH);
    $query = parse_url($url, PHP_URL_QUERY);
    $acl = $query ? $path . '?' . $query. '*' : $path . '*';
    $c = $this->akamai_config;
    $g = $this->akamai_generate;
    $c->set_debug((bool) $config->get('debug'));
    $c->set_window((int) $config->get('token_time'));
    $c->set_start_time($timestamp);
    $c->set_acl($acl);
    $c->set_algo('sha256');
    $c->set_key((string) $key);
    $token = $g->generate_token($c);
    $url = trim($url, '?');

    if (strpos($url, "?") === FALSE) {
      $return_url = sprintf("%s?%s=%s", $url, $tokenParameterName, urlencode($token));
    }
    else {
      $return_url = sprintf("%s&%s=%s", $url, $tokenParameterName, urlencode($token));
    }

    return $return_url;
  }

}
