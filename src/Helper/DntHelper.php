<?php

namespace Drupal\wedia\Helper;

/**
 * Helper to add the "Do not track" URL parameter
 * inside the "src" iframe attribute of a Wedia code emebed asset.
 */
class DntHelper {

  /**
   * Add "dnt" URL parameter in iframe src attribute inside the html code.
   *
   * @param string $html
   *   The code embed to update.
   *
   * @return string
   */
  public function addToHtml($html) {
    $new_html = $html;
    $html = str_replace(['<iframe ', '></iframe>'], '', $html);
    $attributes = explode(' ', $html);
    foreach ($attributes as $key => $attr) {
      if (substr($attr, 0, 4) == 'src=') {
        $attributes[$key] = substr_replace($attributes[$key], '', -1);
        $attributes[$key] .= '&amp;dnt=1"';
      }
    }
    $new_html = implode(' ', $attributes);
    $new_html = '<iframe ' . $new_html . '></iframe>';
    return $new_html;
  }

}
