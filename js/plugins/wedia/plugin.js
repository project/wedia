/**
 * @file
 * Wedia Embed Plugin.
 */

(function () {

	CKEDITOR.plugins.add('wedia', {
		lang: [ 'en'],
		init: function (editor) {
			editor.addCommand('wedia', new CKEDITOR.dialogCommand('wedia', {
				allowedContent: 'div{*}(*); iframe{*}[!width,!height,!src,!frameborder,!allowfullscreen,!allow]; object param[*]; a[*]; img[*]'
			}));

			editor.ui.addButton('Wedia', {
				label : editor.lang.wedia.button,
				toolbar : 'insert',
				command : 'wedia',
				icon : this.path + 'images/logo.png'
			});

			CKEDITOR.dialog.add('wedia', function (instance) {
				var video, disabled = editor.config.wedia_disabled_fields || [];

				return {
					title : editor.lang.wedia.title,
					minWidth : window.screen.width - 100,
					minHeight : window.screen.height - 300,

					onShow: function () {
						var iframe = document.querySelector('iframe[src*="pickerIdentifier"]');
            iframe.style.height = (window.screen.height - 200) + 'px';
						editorInstance = this.getParentEditor();
					},
					contents :
						[{
							id : 'wediaPlugin',
							expand : true,
							elements :
								[{
									type : 'html',
									html : '<div id="my-content-id">\n' +
										'        <div id="main-template" class="gridcontainer">\n' +
										'            <main>\n' +
										'                <div class="panel-container">\n' +
										'                    <div class="panel-right">\n' +
										'                        <div id="wedia-asset-picker" class="wedia-asset-picker"></div>\n' +
										'                    </div>\n' +
										'                </div>\n' +
										'            </main>\n' +
										'        </div>\n' +
										'    </div>',
									onLoad: function () {
										loadToDialog('#wedia-asset-picker');
									}
								}]
						}
					],
					onOk: function()
					{
						var element = CKEDITOR.dom.element.createFromHtml(content);
						var instance = this.getParentEditor();
						instance.insertElement(element);
					}
				};
			});
		}
	});
})();
