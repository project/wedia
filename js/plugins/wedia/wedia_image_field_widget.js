/**
 * @file
 * Manage the display of Wedia Content Picker and the Wedia Image field values.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Retreive the image alternative text.
   *
   * @name getWediaImgAlt
   * @function
   * @param {array} metadata
   *   Image metadata returned by the content picker.
   * @returns {string}
   */
  function getWediaImgAlt(entity) {
    var metadata = entity.wedia_metadata;
    // Build attribute name with current language.
    var name_lang = 'name' + drupalSettings.Wedia.lang;
    var alt_text = '';
    if (metadata.hasOwnProperty(name_lang) && metadata[name_lang].length > 0) {
      alt_text = metadata[name_lang];
    }
    else if (metadata.hasOwnProperty('name') && metadata.name.length > 0) {
      alt_text = metadata.name;
    }
    else {
      alt_text = entity.title;
    }
    return alt_text;
  }

  /**
   * Retreive the image file name.
   *
   * @name getWediaImgFileName
   * @function
   * @param {object} entity
   *   The image returned by the content picker.
   * @param {boolean} is_file_custom_name
   *   Wedia Drupal module configuration flag for add keywords to the file name.
   * @param {string} file_custom_name_property
   *   Wedia Drupal module configuration string to use as key of metadata to use in file name.
   * @returns {string}
   */
  function getWediaImgFileName(entity, is_file_custom_name, file_custom_name_property) {
    var metadata = entity.wedia_metadata;
    var name_lang = 'name' + drupalSettings.Wedia.lang;
    var file_name = '';
    // Entity attribute to use for the first part of the name.
    if (metadata.hasOwnProperty(name_lang) && metadata[name_lang].length > 0) {
      file_name = metadata[name_lang];
    }
    else if (metadata.hasOwnProperty('name') && metadata.name.length > 0) {
      file_name = metadata.name;
    }
    else {
      file_name = entity.title;
    }
     // Check if custom metadata name needed exists.
    if (is_file_custom_name && metadata.hasOwnProperty(file_custom_name_property)) {
      // Custom feature if keywords.
      if (file_custom_name_property == 'keywords') {
        var keywords = metadata['keywords'];
        $.each(keywords, function (index, keyword) {
          if (index != (keywords.length)) {
            file_name += '-';
          }
          file_name += keyword['name'];
        });
      }
      // File name is metadata value.
      else {
        file_name += '-';
        file_name += metadata[file_custom_name_property];
      }
    }
    // Remove accents/diacritics.
    file_name = file_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    // Remove all spaces.
    file_name = file_name.replace(/ /g,"-");

    return file_name.toLowerCase();
  }

  /**
   * Clear all sub-field values off a Wedia image field.
   *
   * @name resetImageField
   * @function
   * @param {object} field
   *   Field DOM element.
   * @returns {string}
   */
  function resetImageField(field) {
    var field_uri = field.find('.wedia-img-uri');
    var field_alt = field.find('.wedia-img-alt');
    var field_width = field.find('.wedia-img-width');
    var field_height = field.find('.wedia-img-height');
    var field_preview = field.find('.wedia-preview');
    // Reset preview.
    field_preview.empty();
    // Reset fields.
    field_uri.val('');
    field_alt.val('');
    field_width.val('');
    field_height.val('');
  }

  /**
   * Get Wedia image field widget wrapper.
   *
   * @name getField
   * @function
   * @param {object} element
   *   DOM element.
   * @returns {object}
   */
  function getImageField(element) {
    return element.parents('.field--widget-wedia-image-field-widget');
  }

  /**
   * Attaches a Javascript Drupal behavior to Wedia image field widget.
   */
  Drupal.behaviors.WediaImageField = {
    attach: function () {
      // Retrieve Drupal settings.
      var debug = drupalSettings.Wedia.debug;
      var is_file_custom_name = drupalSettings.Wedia.is_file_custom_name;
      var file_custom_name_property = drupalSettings.Wedia.file_custom_name_property;
      var file_resolution = drupalSettings.Wedia.file_resolution;
      // Image variation.
      var variation = null;
      if (drupalSettings.Wedia.variation) {
        var variation = drupalSettings.Wedia.variation;
      }
      // Button to close the popin.
      $('.wedia-content-picker-close').on('click', function(event) {
        var field = getImageField($(this));
        field.find('.wedia-content-picker').removeClass('is_open');
        field.find('.wedia-content-picker-iframe').empty();
      });
      // Clear field values.
      $('.wedia-img-clear-action').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        var field = getImageField($(this));
        resetImageField(field);
      });
      // Open the Wedia content picker popin.
      $('input.wedia-img-insert-button').on('click', function(event) {
        event.preventDefault();
        // Current field sub-fields.
        var field = getImageField($(this));
        var field_alt = field.find('.wedia-img-alt');
        var field_uri = field.find('.wedia-img-uri');
        var field_width = field.find('.wedia-img-width');
        var field_height = field.find('.wedia-img-height');
        var field_preview = field.find('.wedia-preview');
        var field_options = field.find('.wedia-img-options');
        var field_options_name = field_options.find('input').first().attr('name');
        var option_selected = field.find('input[name="'+field_options_name+'"]:checked').val();
        var is_error = false;
        // Check image dimensions are defined.
        if (option_selected == 'crop' && field_width.val() == '') {
          field_width.addClass('error');
          field_width.parent().find('label').addClass('form-required has-error');
          is_error = true;
        }
        else {
          field_width.removeClass('error');
          field_width.parent().find('label').removeClass('form-required has-error');
        }
        if (option_selected == 'crop' && field_height.val() == '') {
          field_height.addClass('error');
          field_height.parent().find('label').addClass('form-required has-error');
          is_error = true;
        }
        else {
          field_height.removeClass('error');
          field_height.parent().find('label').removeClass('form-required has-error');
        }
        if (is_error) {
          return;
        }
        // Show cropper.
        var is_show_cropper = '0';
        if (option_selected == 'crop') {
          is_show_cropper = '1';
        }
        // Show focus.
        var is_show_focus = '0';
        if (option_selected == 'focus') {
          is_show_focus = '1';
        }
        // DOM elements needed.
        var popin = field.find('.wedia-content-picker');
        var iframe = field.find('.wedia-content-picker-iframe');
        // Display the content picker popin.
        popin.addClass('is_open');
        // Define content picker options.
        var assetPickerOpts = {
          lang: drupalSettings.Wedia.lang,
          server: drupalSettings.Wedia.server_url,
          assetNatures: 'IMAGE',
          showPreview: drupalSettings.Wedia.preview,
          showFilters: drupalSettings.Wedia.filters,
          showSort: drupalSettings.Wedia.sort,
          showCursors: drupalSettings.Wedia.cursors,
          showCropper: is_show_cropper,
          showFocus: is_show_focus,
          configPath: '',
          min: '',
          max: '',
          el: iframe[0],
          onPick: function (asset) {
            resetImageField(field);
            // Transform picker asset object to array.
            var container = new Array();
            container.push(asset);
            // Get asset entity.
            var entity = container[0].entities[0];
            if (debug == true) {
              console.log(entity);
            }
            // Get image URL.
            var wedia_image_url = entity.url;
            // Generate file name.
            var file_name = getWediaImgFileName(entity, is_file_custom_name, file_custom_name_property);
            wedia_image_url = wedia_image_url.replace('/out', '/' + file_name);
            // Display preview.
            field_preview.append(
              $('<img>')
              .attr('src', entity.url)
              .attr('height', '200')
              .addClass('wedia-preview-img')
            );
            // Set image URL in Wedia image link URL field.
            field_uri.val(wedia_image_url);
            // Set image width.
            field_width.val(entity.width);
            // Set image height.
            field_height.val(entity.height);
            // Set image alternative text in Wedia image text field.
            field_alt.val(getWediaImgAlt(entity));
            // Close content picker.
            popin.removeClass('is_open');
            // Remove iframe element.
            iframe.empty();
          },
        };
        if (is_show_cropper == '1') {
          assetPickerOpts.expectedWidth = field_width.val();
          assetPickerOpts.expectedHeight = field_height.val();
        }
        if (variation) {
          assetPickerOpts.variations = variation;
        }

        WediaContentPicker.attach(assetPickerOpts);

        return false;
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
