/**
 * @file
 * Manage the display of Wedia Content Picker and the Wedia Image field values.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Parse string as HTML code to update width and height.
   *
   * @name updateVideoSizeInIframeHTML
   * @function
   * @param {string} html
   *   String reprensenting the iframe HTML object.
   * @param {string} width
   *   New width to apply to the iframe.
   * @param {string} height
   *   New height to apply to the iframe.
   * @returns {string}
   */
  function updateVideoSizeInIframeHTML(html, width, height) {
    if (!html) {
      return '';
    }
    var temp = $.parseHTML(html);
    temp[0]['attributes']['width']['value'] = width;
    temp[0]['attributes']['height']['value'] = height;
    var temp_src = temp[0]['attributes']['src']['value'];
    var src = temp_src;
    src = updateURLParameter(src, 'Width', width + 'px');
    src = updateURLParameter(src, 'Height', height + 'px');
    temp[0]['attributes']['src']['value'] = src;
    return temp[0]['outerHTML'];
  }

  /**
   * Add or update existing URL parameter.
   *
   * @name updateURLParameter
   * @function
   * @param {string} url
   *   Full URL to update.
   * @param {string} param
   *   URL parameter name.
   * @param {string} paramVal
   *   URL parameter value.
   * @returns {string}
   */
  function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i = 0; i < tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
  }

  /**
   * Clear all sub-field values off a Wedia video field.
   *
   * @name resetVideoField
   * @function
   * @param {object} field
   *   Field DOM element.
   * @returns {string}
   */
  function resetVideoField(field) {
    var field_html = field.find('textarea.wedia-video-html');
    var field_size = field.find('.wedia-video-size');
    var field_width = field.find('.wedia-video-width');
    var field_height = field.find('.wedia-video-height');
    var field_preview = field.find('.wedia-video-preview');
    // Reset preview.
    field_preview.addClass('hidden').empty();
    // Reset fields.
    field_html.val('');
    field_html.empty();
    field_width.val('');
    field_width.parent().addClass('hidden');
    field_height.val('');
    field_height.parent().addClass('hidden');
    field_size.val(
      field_size.find('option:first').val()
    );
  }

  /**
   * Get Wedia video field widget wrapper.
   *
   * @name getField
   * @function
   * @param {object} element
   *   DOM element.
   * @returns {object}
   */
  function getVideoField(element) {
    return element.parents('.field--widget-wedia-video-field-widget');
  }

  /**
   * Attaches a Javascript Drupal behavior to Wedia video field widget.
   */
  Drupal.behaviors.WediaVideoField = {
    attach: function () {
      var debug = drupalSettings.Wedia.debug;
      // Button to close the popin.
      $('.wedia-content-picker-close').on('click', function(event) {
        var field = getVideoField($(this));
        field.find('.wedia-content-picker').removeClass('is_open');
        field.find('.wedia-content-picker-iframe').empty();
      });
      // Clear field values.
      $('.wedia-video-clear-action').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        var field = getVideoField($(this));
        resetVideoField(field);
      });
      // Hide custom width and height inputs if preset size selected.
      $('select.wedia-video-size').each(function(index, select_size) {
        var field = getVideoField($(select_size));
        var fields_size_custom = field.find('.wedia-size-custom');
        var size = $(select_size).val();
        if (size == 'custom') {
          fields_size_custom.parent().removeClass('hidden');
        }
        else {
          fields_size_custom.parent().addClass('hidden');
        }
      });
      // Update of video size select form element.
      $('select.wedia-video-size').on('change', function(event) {
        var field = getVideoField($(this));
        var fields_size_custom = field.find('.wedia-size-custom');
        var field_html = field.find('textarea.wedia-video-html');
        var size = $(this).val();
        if (size == 'custom') {
          fields_size_custom.parent().removeClass('hidden');
        }
        else {
          fields_size_custom.parent().addClass('hidden');
          var size_split = size.split('_');
          var width = size_split[0];
          var height = size_split[1];
          field.find('input.wedia-video-width').val(width);
          field.find('input.wedia-video-height').val(height);
          field_html.val(
            updateVideoSizeInIframeHTML(field_html.val(), width, height)
          );
        }
      });
      // Update video width in code embed.
      $('.wedia-video-width').on('keyup', function(event) {
        var field = getVideoField($(this));
        var field_html = field.find('textarea.wedia-video-html');
        field_html.val(
          updateVideoSizeInIframeHTML(field_html.val(), $(this).val(), field.find('.wedia-video-height').val())
        );
      });
      // Update video height in code embed.
      $('.wedia-video-height').on('keyup', function(event) {
        var field = getVideoField($(this));
        var field_html = field.find('textarea.wedia-video-html');
        field_html.val(
          updateVideoSizeInIframeHTML(field_html.val(), field.find('.wedia-video-width').val(), $(this).val())
        );
      });
      // Open the Wedia content picker popin.
      $('input.wedia-video-insert-button').on('click', function(event) {
        event.preventDefault();
        var field = getVideoField($(this));
        var field_size = field.find('select.wedia-video-size');
        var is_error = false;
        // Check video size is defined.
        if (field_size.val() == '') {
          field_size.addClass('error');
          field_size.parent().find('label').addClass('form-required has-error');
          is_error = true;
        }
        else {
          field_size.removeClass('error');
          field_size.parent().find('label').removeClass('form-required has-error');
        }
        if (is_error) {
          return;
        }
        // DOM elements needed.
        var field_width = field.find('input.wedia-video-width');
        var field_height = field.find('input.wedia-video-height');
        var field_html = field.find('textarea.wedia-video-html');
        var popin = field.find('.wedia-content-picker');
        var iframe = field.find('.wedia-content-picker-iframe');
        // Display the content picker popin.
        popin.addClass('is_open');
        // Get video widht and height.
        var size = field_size.val();
        var width = '560';
        var height = '315';
        if (size == 'custom') {
          if (field_width.val()) {
            width = field_width.val();
          }
          if (field_height.val()) {
            height = field_height.val();
          }
        }
        else {
          var size_split = size.split('_');
          width = size_split[0];
          height = size_split[1];
        }
        // Define content picker options.
        var assetPickerOpts = {
          lang: drupalSettings.Wedia.lang,
          server: drupalSettings.Wedia.server_url,
          assetNatures: 'VIDEO',
          showPreview: drupalSettings.Wedia.preview,
          showFilters: drupalSettings.Wedia.filters,
          showSort: drupalSettings.Wedia.sort,
          showCursors: drupalSettings.Wedia.cursors,
          showCropper: '0',
          showFocus: '0',
          configPath: '',
          variations: 'ia_preview_video_source,thumbnailBigAuto',
          min: '',
          max: '',
          el: iframe[0],
          onPick: function (asset) {
            // Transform picker asset object to array.
            var container = new Array();
            container.push(asset);
            // Get asset entity.
            var entity = container[0].entities[0];
            if (debug == true) {
              console.log(entity);
            }
            var html = entity['html'];
            // Display preview.
            field.find('div.wedia-video-preview').removeClass('hidden').html(html);
            // Set video size.
            field_width.val(entity.width);
            field_height.val(entity.height);
            // Set iframe HTML in Wedia video html field.
            field_html.empty().val(updateVideoSizeInIframeHTML(html, width, height));
            // Close content picker.
            popin.removeClass('is_open');
            // Remove iframe element.
            iframe.empty();
          },
        };

        WediaContentPicker.attach(assetPickerOpts);

        return false;
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
