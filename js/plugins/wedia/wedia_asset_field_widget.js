/**
 * @file
 * Manage the display of Wedia Content Picker
 * and the Wedia generic asset field values.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Parse string as HTML code to update width and height.
   *
   * @name updateVideoSizeInIframeHTML
   * @function
   * @param {string} html
   *   String reprensenting the iframe HTML object.
   * @param {string} width
   *   New width to apply to the iframe.
   * @param {string} height
   *   New height to apply to the iframe.
   * @returns {string}
   */
  function updateVideoSizeInIframeHTML(html, width, height) {
    if (!html) {
      return '';
    }
    var temp = $.parseHTML(html);
    temp[0]['attributes']['width']['value'] = width;
    temp[0]['attributes']['height']['value'] = height;
    var temp_src = temp[0]['attributes']['src']['value'];
    var src = temp_src;
    src = updateURLParameter(src, 'Width', width + 'px');
    src = updateURLParameter(src, 'Height', height + 'px');
    temp[0]['attributes']['src']['value'] = src;
    return temp[0]['outerHTML'];
  }

  /**
   * Retreive the image alternative text.
   *
   * @name getWediaImgAlt
   * @function
   * @param {array} metadata
   *   Image metadata returned by the content picker.
   * @returns {string}
   */
  function getWediaImgAlt(entity) {
    var metadata = entity.wedia_metadata;
    // Build attribute name with current language.
    var name_lang = 'name' + drupalSettings.Wedia.lang;
    var alt_text = '';
    if (metadata.hasOwnProperty(name_lang) && metadata[name_lang].length > 0) {
      alt_text = metadata[name_lang];
    }
    else if (metadata.hasOwnProperty('name') && metadata.name.length > 0) {
      alt_text = metadata.name;
    }
    else {
      alt_text = entity.title;
    }
    return alt_text;
  }

  /**
   * Retreive the image file name.
   *
   * @name getWediaImgFileName
   * @function
   * @param {object} entity
   *   The image returned by the content picker.
   * @param {boolean} is_file_custom_name
   *   Wedia Drupal module configuration flag for add keywords to the file name.
   * @param {string} file_custom_name_property
   *   Wedia Drupal module configuration string to use as key of metadata to use in file name.
   * @returns {string}
   */
  function getWediaImgFileName(entity, is_file_custom_name, file_custom_name_property) {
    var metadata = entity.wedia_metadata;
    var name_lang = 'name' + drupalSettings.Wedia.lang;
    var file_name = '';
    // Entity attribute to use for the first part of the name.
    if (metadata.hasOwnProperty(name_lang) && metadata[name_lang].length > 0) {
      file_name = metadata[name_lang];
    }
    else if (metadata.hasOwnProperty('name') && metadata.name.length > 0) {
      file_name = metadata.name;
    }
    else {
      file_name = entity.title;
    }
     // Check if custom metadata name needed exists.
    if (is_file_custom_name && metadata.hasOwnProperty(file_custom_name_property)) {
      // Custom feature if keywords.
      if (file_custom_name_property == 'keywords') {
        var keywords = metadata['keywords'];
        $.each(keywords, function (index, keyword) {
          if (index != (keywords.length)) {
            file_name += '-';
          }
          file_name += keyword['name'];
        });
      }
      // File name is metadata value.
      else {
        file_name += '-';
        file_name += metadata[file_custom_name_property];
      }
    }
    // Remove accents/diacritics.
    file_name = file_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    // Remove all spaces.
    file_name = file_name.replace(/ /g,"-");

    return file_name.toLowerCase();
  }

  /**
   * Add or update existing URL parameter.
   *
   * @name updateURLParameter
   * @function
   * @param {string} url
   *   Full URL to update.
   * @param {string} param
   *   URL parameter name.
   * @param {string} paramVal
   *   URL parameter value.
   * @returns {string}
   */
  function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i = 0; i < tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
  }

  /**
   * Clear all sub-field values off a Wedia generic asset field.
   *
   * @name resetAssetField
   * @function
   * @param {object} field
   *   Field DOM element.
   * @returns {string}
   */
  function resetAssetField(field) {
    var field_uri = field.find('.wedia-asset-uri');
    var field_alt = field.find('.wedia-asset-alt');
    var field_type = field.find('.wedia-asset-type');
    var field_size = field.find('.wedia-asset-video-size');
    var field_width = field.find('.wedia-asset-width');
    var field_height = field.find('.wedia-asset-height');
    var field_preview = field.find('.wedia-asset-preview');
    var field_code_embed = field.find('.wedia-asset-code-embed');
    // Reset preview.
    field_preview.empty();
    // Reset fields.
    field_uri.val('');
    field_width.val('');
    field_height.val('');
    field_alt.val('');
    field_code_embed.val('');
    field_size.val(
      field_size.find('option:first').val()
    );
    // Hide fields.
    field_alt.parent().addClass('hidden');
    field_size.parent().addClass('hidden');
    field_width.parent().addClass('hidden');
    field_height.parent().addClass('hidden');
  }

  /**
   * Get Wedia generic asset field widget wrapper.
   *
   * @name getField
   * @function
   * @param {object} element
   *   DOM element.
   * @returns {object}
   */
  function getAssetField(element) {
    return element.parents('.field--widget-wedia-asset-field-widget');
  }

  /**
   * Attaches a Javascript Drupal behavior to Wedia generic asset field widget.
   */
  Drupal.behaviors.WediaAssetField = {
    attach: function () {
      var debug = drupalSettings.Wedia.debug;
      // Button to close the popin and remove content picker iframe DOM element.
      $('.wedia-content-picker-close').on('click', function(event) {
        var field = getAssetField($(this));
        field.find('.wedia-content-picker').removeClass('is_open');
        field.find('.wedia-content-picker-iframe').empty();
      });
      // Apply specific features on all fields according the asset type.
      $('.field--widget-wedia-asset-field-widget').each(function() {
        var field = $(this);
        var field_uri = field.find('.wedia-asset-uri');
        var field_alt = field.find('.wedia-asset-alt');
        var field_type = field.find('.wedia-asset-type');
        var field_size = field.find('.wedia-asset-video-size');
        var field_width = field.find('.wedia-asset-width');
        var field_height = field.find('.wedia-asset-height');
        // Hide all sub-fields.
        field_alt.parent().addClass('hidden');
        field_size.parent().addClass('hidden');
        field_width.parent().addClass('hidden');
        field_height.parent().addClass('hidden');
        // Check asset type.
        var asset_type = field_type.val();
        // Image asset type features.
        if (asset_type == 'photo') {
          // Update expected width and height.
          field_width.on('keyup', function(e) {
            field_uri.val(
              updateURLParameter(
                field_uri.val(),
                'expectedWidth',
                $(this).val()
              )
            );
          });
          field_height.on('keyup', function(e) {
            field_uri.val(
              updateURLParameter(
                field_uri.val(),
                'expectedHeight',
                $(this).val()
              )
            );
          });
          // Show image sub-fileds.
          field_alt.parent().removeClass('hidden');
          field_width.parent().removeClass('hidden');
          field_height.parent().removeClass('hidden');
        }
        // Video asset type features.
        else {
          // Show video size.
          field_size.parent().removeClass('hidden');
          // Show width and height if custom size selected.
          var size = field_size.val();
          if (size == 'custom') {
            field_width.parent().removeClass('hidden');
            field_height.parent().removeClass('hidden');
          }
        }
      });
      // Update of video size select form element.
      $('select.wedia-asset-video-size').on('change', function(event) {
        var field = getAssetField($(this));
        var field_width = field.find('.wedia-asset-width');
        var field_height = field.find('.wedia-asset-height');
        var field_html = field.find('textarea.wedia-asset-code-embed');
        var size = $(this).val();
        if (size == 'custom') {
          field_width.parent().removeClass('hidden');
          field_height.parent().removeClass('hidden');
        }
        else {
          field_width.parent().addClass('hidden');
          field_height.parent().addClass('hidden');
          var size_split = size.split('_');
          var width = size_split[0];
          var height = size_split[1];
          field_width.val(width);
          field_height.val(height);
          field_html.val(
            updateVideoSizeInIframeHTML(field_html.val(), width, height)
          );
        }
      });
      // Update video width in code embed.
      $('.wedia-asset-width').on('keyup', function(event) {
        var field = getAssetField($(this));
        var field_html = field.find('textarea.wedia-asset-code-embed');
        field_html.val(
          updateVideoSizeInIframeHTML(field_html.val(), $(this).val(), field.find('.wedia-asset-height').val())
        );
      });
      // Update video height in code embed.
      $('.wedia-asset-height').on('keyup', function(event) {
        var field = getAssetField($(this));
        var field_html = field.find('textarea.wedia-asset-code-embed');
        field_html.val(
          updateVideoSizeInIframeHTML(field_html.val(), field.find('.wedia-asset-width').val(), $(this).val())
        );
      });
      // Clear field values.
      $('.wedia-asset-clear-action').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        var field = getAssetField($(this));
        resetAssetField(field);
      });
      // Open the Wedia content picker popin.
      $('input.wedia-asset-insert-button').on('click', function(event) {
        // DOM elements needed.
        var field = getAssetField($(this));
        var field_uri = field.find('.wedia-asset-uri');
        var field_alt = field.find('.wedia-asset-alt');
        var field_type = field.find('.wedia-asset-type');
        var field_size = field.find('.wedia-asset-video-size');
        var field_width = field.find('.wedia-asset-width');
        var field_height = field.find('.wedia-asset-height');
        var field_preview = field.find('.wedia-asset-preview');
        var field_code_embed = field.find('.wedia-asset-code-embed');
        var popin = field.find('.wedia-content-picker');
        var iframe = field.find('.wedia-content-picker-iframe');
        // Drupal settings.
        var is_file_custom_name = drupalSettings.Wedia.is_file_custom_name;
        var file_custom_name_property = drupalSettings.Wedia.file_custom_name_property;
        var file_resolution = drupalSettings.Wedia.file_resolution;
        // Display the content picker popin.
        popin.addClass('is_open');
        // Define content picker options.
        var assetPickerOpts = {
          lang: drupalSettings.Wedia.lang,
          server: drupalSettings.Wedia.server_url,
          assetNatures: 'ALL',
          showPreview: drupalSettings.Wedia.preview,
          showFilters: drupalSettings.Wedia.filters,
          showSort: drupalSettings.Wedia.sort,
          showCursors: drupalSettings.Wedia.cursors,
          showCropper: '1',
          showFocus: '0',
          configPath: '',
          min: '',
          max: '',
          el: iframe[0],
          onPick: function (asset) {
            // Reset field values.
            resetAssetField(field);
            field.find('.wedia-asset-clear-action').removeClass('hidden');
            // Transform picker asset object to array.
            var container = new Array();
            container.push(asset);
            // Get asset entity.
            var entity = container[0].entities[0];
            if (debug == true) {
              console.log(entity);
            }
            var type = entity.type;
            field_type.val(type);
            if (type == 'photo') {
              // Show image fields.
              field_alt.parent().removeClass('hidden');
              field_width.parent().removeClass('hidden');
              field_height.parent().removeClass('hidden');
              // Get image URL.
              var wedia_image_url = entity.url;
              // Generate file name.
              var file_name = getWediaImgFileName(entity, is_file_custom_name, file_custom_name_property);
              wedia_image_url = wedia_image_url.replace('/out', '/' + file_name);
              // Display preview.
              field_preview.append(
                $('img')
                .attr("src", wedia_image_url)
                .attr("width", '200')
                .attr("height", '200')
              );
              // Set image URL in Wedia image link URL field.
              field_uri.val(wedia_image_url);
              // Set image width.
              field_width.val(entity.width);
              // Set image height.
              field_height.val(entity.height);
              // Set image alternative text in Wedia image text field.
              field_alt.val(getWediaImgAlt(entity));
            }
            else {
              // Show video fields.
              field_size.val(field_size.find('option:first').val());
              field_size.parent().removeClass('hidden');
              // Display preview.
              field_preview.html(entity.html);
              // Set video width.
              field_width.val(entity.width);
              // Set video height.
              field_height.val(entity.height);
              // Set code embed.
              field_code_embed.val(entity.html);
            }
            // Close content picker popin.
            popin.removeClass('is_open');
            // Remove iframe DOM element.
            iframe.empty();
          },
        };

        WediaContentPicker.attach(assetPickerOpts);

        return false;
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
