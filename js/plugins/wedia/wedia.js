/**
 * @file
 * Wedia content picker.
 */

var editorInstance;

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.wediaAccessData = {
    // Load on dialog.
  };

  // Load Wedia Drupal module configuration. 
  var formats = null;
  var editor_settings = null;
  if (drupalSettings.hasOwnProperty('editor') && drupalSettings.editor.hasOwnProperty('formats')) {
    var formats = drupalSettings.editor.formats;
    if (formats.hasOwnProperty('basic_html')) {
      var editor_settings = drupalSettings.editor.formats.basic_html.editorSettings;
    }
    if (formats.hasOwnProperty('full_html')) {
      var editor_settings = drupalSettings.editor.formats.full_html.editorSettings;
    }
  }

  /**
   * Retreive the image alternative text.
   *
   * @name getAlternativeText
   * @function
   * @param {object} entity
   *   The image entity.
   * @returns {string}
   */
  function getAlternativeText(entity) {
    var metadata = entity.wedia_metadata;
    // Build attribute name with current language.
    var name_lang = 'name' + editor_settings.lang;
    if (metadata.hasOwnProperty(name_lang) == false) {
      return metadata['name'];
    }
    else {
      return metadata[name_lang];
    }
  }

  /**
   * Retreive the image file name.
   *
   * @name buildFileName
   * @function
   * @param {object} metadata
   *   The image metadata.
   * @returns {string}
   */
  function buildFileName(metadata) {
    // Check if custom file name is needed.
    if (editor_settings.is_file_custom_name == false) {
      return false;
    }
    // Check if custom metadata name needed exists.
    if (metadata.hasOwnProperty(editor_settings.file_custom_name_property) == false) {
      return false;
    }
    // Build file name from asset metadata.
    var file_name = '';
    // Custom feature if keywords.
    if (editor_settings.file_custom_name_property == 'keywords') {
      var keywords = metadata['keywords'];
      $.each(keywords, function (index, keyword) {
        file_name += keyword['name'];
        // Add keyword separator.
        if (index != (keywords.length -1)) {
          file_name += '_';
        }
      });
    }
    // File name is metadata value.
    else {
      file_name = metadata[editor_settings.file_custom_name_property];
    }
    // Remove accents/diacritics.
    return file_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

  /**
   * Load Wedia content picker.
   *
   * @name loadWediaContentPicker
   * @function
   * @param {object} selector
   * @param {*} logSelector
   * @returns {void}
   */
  function loadWediaContentPicker(selector, logSelector) {
    // DOM element where content picker is attached.
    var pickerParentDomItem = document.querySelector(selector);
    // Show cropper.
    var is_show_cropper = '0';
    if (editor_settings.ck_editor_options == 'crop') {
      is_show_cropper = '1';
    }
    // Show focus.
    var is_show_focus = '0';
    if (editor_settings.ck_editor_options == 'focus') {
      is_show_focus = '1';
    }
    // Content picker options.
    var assetPickerOpts = {
      server: editor_settings.server_url,
      assetNatures: '',
      showPreview: editor_settings.preview,
      showFilters: editor_settings.filters,
      showSort: editor_settings.sort,
      showCursors: editor_settings.cursors,
      showCropper: is_show_cropper,
      showFocus: is_show_focus,
      configPath: '',
      min: '',
      max: '',
      el: pickerParentDomItem,
      onPick: function (asset) {
        // Transform picker asset object to array.
        var container = new Array();
        container.push(asset);
        // Get asset entity.
        var entity = container[0].entities[0];
        if (editor_settings.debug == true) {
          console.log(entity);
        }
        // Asset type (ALL,IMAGE, VIDEO, 3D, AUDIO, DOCUMENT, PRINT, ZIP).
        var asset_type = entity.type;
        // Image asset.
        if(asset_type == 'photo') {
          // Get variation.
          if(entity.wedia_variations[1] && entity.wedia_variations[1].name == 'cropped'){
            var wedia_image_url = entity.wedia_variations[1].url;
          }
          else {
            var wedia_image_url = entity.wedia_variations[0].url;
          }
          // Custom file name.
          var file_name = buildFileName(entity.wedia_metadata);
          if (file_name != false) {
            wedia_image_url = wedia_image_url.replace('/out', '/' + file_name);
          }
          // Build HTML element.
          var pasteThis = $('<p>').append
          (
            $('<img>')
            .attr('src', wedia_image_url)
            .attr('alt', getAlternativeText(entity))
            .attr('width', entity.width)
            .attr('height', entity.height)
          ).html();
        }
        // Video asset.
        if(asset_type == 'video'){
          var pasteThis = $('<p>').append( entity.html).html();
        }
        // Rich asset.
        if(asset_type == 'rich'){
          var pasteThis = $('<p>').append( entity.html).html();
        }
        // Add HTML element in the source of current Drupal field.
        var element = CKEDITOR.dom.element.createFromHtml(pasteThis);
        editorInstance.insertElement(element);
        // Close dialog.
        CKEDITOR.dialog.getCurrent().hide();
      },
    };
    // Apply expected image dimension if crop option is enabled.
    if (is_show_cropper == '1') {
      assetPickerOpts.expectedWidth = editor_settings.ck_editor_width;
      assetPickerOpts.expectedHeight = editor_settings.ck_editor_height;
    }
    // Apply image variation if variation is enabled.
    if (editor_settings.default_variation) {
      assetPickerOpts.variations = editor_settings.default_variation;
    }

    WediaContentPicker.attach(assetPickerOpts);
  }

  /**
   * Overive Drupal dialog modal.
   *
   * @name loadToDialog
   * @function
   * @param {object} selector
   * @param {*} logSelector
   * @returns {void}
   */
  loadToDialog = function (selector, logSelector) {
    loadWediaContentPicker(selector, logSelector)
  }

})(jQuery, Drupal, drupalSettings);
